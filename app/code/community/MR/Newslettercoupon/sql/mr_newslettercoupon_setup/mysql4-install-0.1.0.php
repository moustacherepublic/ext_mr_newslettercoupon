<?php
/**
 * Created by PhpStorm.
 * User: tony
 * Date: 11/02/15
 * Time: 7:15 PM
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable('newsletter_subscriber'),
    'coupon_sent', 'VARCHAR(150) AFTER subscriber_confirm_code');


//create coupon
//get all customer group ids
$customer_groups =  Mage::getModel('customer/group')->getCollection()->getColumnValues('customer_group_id');

$rule = Mage::getModel('salesrule/rule');
$rule->setName('$10 off on first signup')
    ->setDescription('$10 off on first signup')
    ->setFromDate('')
    ->setCouponType(2)
    ->setUsesPerCoupon(1)
    ->setUseAutoGeneration(1)
    ->setUsesPerCustomer(1)
    ->setCustomerGroupIds($customer_groups) //an array of customer grou pids
    ->setIsActive(1)
    ->setConditionsSerialized('')
    ->setActionsSerialized('')
    ->setStopRulesProcessing(0)
    ->setIsAdvanced(1)
    ->setProductIds('')
    ->setSortOrder(0)
    ->setSimpleAction('cart_fixed')
    ->setDiscountAmount(10)
    ->setDiscountQty(null)
    ->setDiscountStep(0)
    ->setSimpleFreeShipping('0')
    ->setApplyToShipping(1)
    ->setIsRss(0)
    ->setWebsiteIds(array(1));
$rule->save();

$installer->setConfigData('mr_neslettercoupon/general/coupon_id', $rule->getId());

$installer->endSetup();



