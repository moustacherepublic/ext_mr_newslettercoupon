<?php

class MR_Newslettercoupon_Model_Observer
{
    /**
     * Created by PhpStorm.
     * User: tony
     * Date: 11/02/15
     * Time: 7:23 PM
     */
    public function handleSubscriber($observer)
    {

        $subscriber = $observer->getEvent()->getSubscriber();
        if ($this->canSendCoupon($subscriber)) {
            $coupon = $this->generateCoupon();
            if ($coupon) {
                $this->sendEmail($subscriber, $coupon);
                $subscriber->setCouponSent($coupon);
            }
        } else {
            return $observer;
        }

    }

    public function sendEmail($subscriber, $coupon)
    {
        $templateId = 'mr_newsletter_coupon';
        $emailTemplate = Mage::getModel('core/email_template')->loadDefault($templateId);
        $emailTo = $subscriber->getSubscriberEmail();
        if ($emailTemplate && $emailTemplate->getId() && $emailTo) {
            $customerName = $emailTo;
            $emailTemplateVariables = array(
                'coupon_code' => $coupon,
            );
            $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
            $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');
            $templateSubject = 'Thank you for your subscription';
            $emailTemplate->setSenderName($senderName);
            $emailTemplate->setSenderEmail($senderEmail);
            $emailTemplate->setTemplateSubject($templateSubject);
            $emailTemplate->send($emailTo, $customerName, $emailTemplateVariables);
        }
    }

    public function canSendCoupon($subscriber)
    {
        if (!$subscriber->getCouponSent() && $subscriber->getStatus() == 1) {
            return true;
        }

        return false;
    }


    public function generateCoupon()
    {
        $ruleId = Mage::getStoreConfig('mr_neslettercoupon/general/coupon_id');
        $rule = Mage::getModel('salesrule/rule')->load($ruleId);
        $couponCode = '';
        if ($rule && $rule->getId()) {
            $massGenerator = $rule->getCouponMassGenerator();
            $session = Mage::getSingleton('core/session');
            try {
                $massGenerator->setData(array(
                    'rule_id' => $ruleId,
                    'qty' => 1,
                    'length' => 12,
                    'format' => 'alphanum',
                    'prefix' => '',
                    'suffix' => '',
                    'dash' => 0,
                    'uses_per_coupon' => 1,
                    'uses_per_customer' => 1
                ));
                $massGenerator->generatePool();
                $latestCuopon = max($rule->getCoupons());
                $couponCode = $latestCuopon->getCode();
            } catch (Exception $e) {
                $session->addException($e, 'There was a problem with coupon generation: ' . $e->getMessage());
                Mage::logException($e);
            }
        }
        return $couponCode;

    }
}
